FROM node:latest as node
MAINTAINER Anton Fofanov
WORKDIR /app
COPY . .
RUN npm install && npm cache clear --force && npm run build --prod

FROM nginx:alpine
COPY --from=node /app/dist/kitchen-sink /usr/share/nginx/html